class ManageElement {
    constructor(id) {
        this.Id = id;
        this.Element = null;

        return this.Get();
    }
    
    static RemoveById(id) {
        let element = document.getElementById(id);

        if (element) {
            element.remove();
        }
    }

    Create(tag) {
        let element = document.createElement(tag);
        element.setAttribute("id", this.Id);

        this.Element = element;
        return element;
    }

    CreateChild(tag, parent = this.Get()) {
        let element = document.createElement(tag)
        parent.appendChild(element);

        return element;
    }

    /**
     *Appends this to the first instanse of element found.
     *
     * @param {*} element
     * @returns
     * @memberof ManageElement
     */
    AppendToElement(element) {
        let pElement = document.getElementsByTagName("body")[0];
        pElement.appendChild(this.Get());

        return this.Get();
    }

    Get() {
        this.Element = this.Element ?? document.getElementById(this.Id);
        return this.Element;
    }

    Remove() {
        if (this.Get()) {
            this.Get().remove();
        }

    }

    Disable() {
        this.Get().disabled = true;
        return this.Get();
    }

    Enable() {
        this.Get().disabled = false;
        return this.Get();
    }

    SetText(text) {
        this.Get().innerHTML = text
        return this.Get();
    }

    
}