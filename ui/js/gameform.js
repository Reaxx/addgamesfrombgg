class GameForm {
    Modal = null;
    FetchButton = null;
    Games = null;

    constructor() {
        this.Modal = new ManageElement("autocompletelist");
        this.FetchButton = new ManageElement("updateBgg");
    }

    async HandleRequest() {
        var gTitle = document.getElementById("gTitle").value;

        //Makes sure that a title was given
        if (!gTitle) {
            console.log("No title given, no request done");
            return;
        }

        //Disables button and sets messege
        this.FetchButton.Disable();
        this.FetchButton.SetText("Fetching..");

        //Makes sure that the modal is closed
        this.Modal.Remove();

        //Disables the fetchbutton to avoid multiple requetsts
        this.FetchButton.innerHTML = "Fetching..";
        // this.FetchButton.disabled = true;

        //Builds URL for fetch
        var url = "./findgame/" + gTitle;

        //Adds param for expension
        if (document.getElementById("gIncExp").checked == true) {
            url += "/expansion";
        }

        //Starts fetch
        var game = await this.MakeRequest(url);

        // var buttonBusyText = "Fetching..";
        // var buttonReadyText = "Fetch from BGG";

        this.HandleResult(game);

        //Disables button and sets messege
        this.FetchButton.Enable();
        this.FetchButton.SetText("Fetch from BGG");
    }

    async MakeRequest(url) {
        var response = await fetch(url);
        var game = await response.json();
        return game;
    }

    HandleResult(result) {
        //Stops if no data is found
        if (result == null) {
            this.FetchButton.innerHTML = "No results found";
            this.FetchButton.disabled = false;
            return this;
        }
        //If only one result, sends directly to form
        else if (result.length == 1) {
            GameForm.FillForm(result[0]);
        } else {
            this.DrawAutocompleteList(result);
        }

        return this;
    }

    /**
     * Autocompletes the form with the given game
     *
     * @static
     * @param {*} game
     * @returns
     * @memberof GameForm
     */
    static FillForm(game) {

        console.log("Filling the form with data");
        console.log(game);

        //If no hit is returned, should really happen
        if (!game) {
            return;
        }

        //Join mechanics into comma-seperated list, if not null
        var mec = null;
        if (game.Mechanics) {
            mec = game.Mechanics.join(", ");
        }

        //Join categoris into comma-seperated list, if not null
        var cat = null;
        if (game.Mechanics) {
            cat = game.Categories.join(", ");
        }


        document.getElementById("gTitle").value = game.Name;
        document.getElementById("gMinPlay").value = game.MinPlayers;
        document.getElementById("gMaxPlay").value = game.MaxPlayers;
        document.getElementById("gLength").value = game.Length;
        document.getElementById("gDesc").value = game.Desc;
        document.getElementById("gId").value = game.BGGId;
        document.getElementById("gImg").value = game.Image;
        document.getElementById("gImgThumb").value = game.ImageThumb;
        document.getElementById("gURL").value = game.BGGUrl;
        document.getElementById("gMec").value = mec;
        document.getElementById("gCat").value = cat;

        console.log("Done filling form");

    }

    DrawAutocompleteList(games) {
        console.log("Draws autocomplete form");

        this.Modal.Create("modal");
        this.Modal.AppendToElement();

        let ul = this.Modal.CreateChild("ul");

        // var modal = document.createElement('modal');
        // modal.setAttribute('id', id);
        // document.body.appendChild(modal);

        // var ul = document.createElement('ul');
        // modal.appendChild(ul);

        //Creates objects to call with the click-event.
        // var fetchGame = new GameForm();
        // var manageElement = new ManageElement(modal.id);

        //Creating a list of all the games
        games.forEach(game => {
            let id = this.Modal.Id;

            let li = this.Modal.CreateChild("li", ul);

            //Adding onClick event to choice game to autocomplete form

            li.addEventListener('click', function () {
                GameForm.FillForm(game);
                ManageElement.RemoveById(id);
            }, false)

            li.innerHTML += "<img src='" + game.ImageThumb + "' class='thumbnail''>";
            li.innerHTML += game.Name;
        });
        console.log("Done drawing autocomplete form");

    }

    // MakeRequest() {
    //     console.log("Starting API-request");

    //     //Closes list if already open.
    //     this.Modal.Remove();

    //     //Disables button
    //     this.FetchButton.innerHTML = buttonBusyText;
    //     this.FetchButton.disabled = true;

    //     // var xmlhttp = new XMLHttpRequest();
    //     var gTitle = document.getElementById("gTitle").value;
    //     var button = document.getElementById("updateBgg");
    //     var incExpan = document.getElementById("gIncExp").checked;

    //     var buttonBusyText = "Fetching..";
    //     var buttonReadyText = "Fetch from BGG";


    //     document.getElementById("gameForm").checkValidity();

    //     //Only makes request if title is given
    //     if (!gTitle) {
    //         console.log("No title given, no request done");
    //         return;
    //     }

    //     //Formats url for the request
    //     var url = "./findgame/" + gTitle;

    //     //Adds expansions if checkbox is checked. Any value after / means include expansions
    //     if (incExpan == true) {
    //         url += "/expansion";
    //     }


    //     fetch(url)
    //         .then(response => response.json())
    //         .then(response => {
    //             // this.FetchButton.innerHTML = buttonReadyText;
    //             // this.FetchButton.disabled = false;
    //             // let Games = response.json;
    //             this.HandleResult(response);
    //             console.log(response);
    //             console.log("Fetch finnished");
    //         });

    // //Sets upp xml
    // xmlhttp.open("GET", url, false);
    // xmlhttp.timeout = 10000;

    // xmlhttp.onloadstart = function () {
    //     //Disables button as loading is started
    //     button.innerHTML = buttonBusyText;
    //     button.disabled = true;
    // }

    // xmlhttp.onloadend = function () {
    //     console.log("onload");
    //     this.Games = JSON.parse(this.responseText);

    // var games = JSON.parse(this.responseText);

    // //If none, stop. If only one result, load into form
    // if (games == null) {
    //     button.innerHTML = "No results found";
    //     button.disabled = false;
    //     return;
    // } else if (games.length == 1) {
    //     FillForm(games);
    // } else {
    //     this.DrawAutocompleteList(games);
    // }

    // //Enables the button
    // button.innerHTML = buttonReadyText;
    // button.disabled = false;
    // }

    // xmlhttp.onerror = function () {
    //     throw new Error("Error");
    // }

    // xmlhttp.ontimeout = function () {
    //     throw new Error("Timeout");
    // }

    // xmlhttp.send();

    //     console.log("API-Request done");
    //     return this;
    // }
}

var gameForm = new GameForm();