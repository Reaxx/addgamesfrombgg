<?php

class GameController
{
    // function FindGame($f3, $params)
    // {
    //     $name = $params["name"];
    //     $game = new Game($name);
    //     $game->LoadGameFromBGG();

    //     var_dump($game);
    // }

    function FindGameAjax($f3, $params)
    {
        $name = $params["name"];
        $showExpansion = array_key_exists("showExpansion",$params); //checks if show expansions is set        

        //returns false if no param found
        if(!$name)
        {
            return null;
        }

        //Creates gamelist
        $gameList = new GameList($name,$showExpansion);

        //Outputs data as json
        echo $gameList;
    }

    function DrawForm($f3)
    {
        echo Template::instance()->render('form.htm');
    }
}

?>