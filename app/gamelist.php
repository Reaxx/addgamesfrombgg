<?php

class GameList
{
    private $List; //Array of games

    public function __construct($name, bool $showExpansions)
    {
        //Gets array of ids
        $ids = BGGApi::FindGameIdsByName($name);

        if(!$ids)
        {
            return false;
        }

        //Turns array into comma-seperated for joint API-request
        $idsCSV = implode(",", $ids);

        //Fetches all ids
        $rawGames = BGGApi::SeachById($idsCSV, $showExpansions);

        //Adds each game to the array
        foreach ($rawGames as $game) {
            $tmpGame = new Game();
            $tmpGame->MapFromXML($game);
            $this->List[] = $tmpGame;
        }
    }

    public function __toString()
    {
        return json_encode($this->List);
    }
}
