<?php

class Game
{
    public $Name;
    public $BGGUrl;
    public $RulesUrl;
    public $Owner;
    public $Genre;
    public $MinPlayers;
    public $MaxPlayers;
    public $Length;
    public $BGGId;
    public $Desc;
    public $Coop;
    public $InGameRoom;
    public $Image;
    public $ImageThumb;
    public $Mechanics;
    public $Categories;

    //The categgories we want to parse in ParseLinks, name of propertie as key. Category as value
    //Avalible: boardgamecategory, boardgamemechanic, boardgamefamily, boardgameexpansion, boardgamedesigner, boardgameartist, boardgamepublisher
    const Categories = array("Categories" => "boardgamecategory", "Mechanics" => "boardgamemechanic" ); 


    function __construct(string $name = null) 
    {
        $this->Name = $name;
    }

    public function MapFromXML($xml)
    {
        $this->ParseLinks($xml);
        
        $this->Name = (string) $xml->name["value"];       
        $this->MinPlayers = (string) $xml->minplayers["value"]; 
        $this->MaxPlayers = (string) $xml->maxplayers["value"];
        $this->Length = (string) $xml->playingtime["value"];       
        $this->Desc = (string) $xml->description;    
        $this->Image = (string) $xml->image;
        $this->ImageThumb = (string) $xml->thumbnail;
        $this->BGGId = (string) $xml["id"];
        $this->BGGUrl = "https://boardgamegeek.com/boardgame/".$xml["id"];

        //$this->Owner = (string) $xml->name["value"];
        // 
        // $this->RulesUrl = (string) $xml->name["value"];
        // $this->Genre = (string) $xml->name["value"];        
        // $this->Coop = (string) $xml->name["value"];
        // $this->InGameRoom = (string) $xml->name["value"];
     }

     //Steps thru the links, parsing the categories set as wanted in const Categories
     private function ParseLinks($xml)
     {         
        foreach ($xml->link as $link) {
            $type = (string)$link["type"];
            $value = (string)$link["value"];

            //Sets propertie
            if(in_array($type, self::Categories))
            {
                $propName = array_search($type, self::Categories);
                $this->$propName[] = $value;
            }           
        }
     }
}

?>