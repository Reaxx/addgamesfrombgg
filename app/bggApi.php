<?php

class BGGApi
{
    private const rootURL = "https://www.boardgamegeek.com/xmlapi2";

    //Finds gameinfo based on ID
    public static function SeachById(string $id,bool $includeExpansion = false)
    {
        $parameter = "/thing?&id=".$id;

        //If we want both games and expansions
        if($includeExpansion)
        {
            $parameter .= "&type=boardgame,boardgameexpansion";
        }
        // Currently not actuly needed, but seems more safe if future types is added
        else {
            $parameter .= "&type=boardgame";
        }

        $result = self::RunQuery($parameter);
        
        return $result;
    }

    // Finns game id based on name, 
    public static function FindGameIdsByName($name, $param = "&type=boardgame")
    {
        //Formats spaces to "+"
        $formName = str_replace(" ","+",$name);
        
        //Expansions show up twice, once as boardgameexpension and once as boardgame with the same ID. So without filtering expansions gets entered twice.
        $parameter = "/search/?query=".$formName.$param;
        $result = self::RunQuery($parameter);

        if(!$result->item)
        {
            return false;
        }

        //Parses returned XML into array of ids
        foreach ($result as $rawId) {
            $ids[] = (string) $rawId["id"];
        }       

        //Only needs to return ID
        return $ids;
    }

    //Concats the query and contacts the API
    public static function RunQuery($parameter)
    {        
        $query = self::rootURL.$parameter;            
        $result = simplexml_load_file($query);

        error_log("test");
        return $result;
    }
}

?>